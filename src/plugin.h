/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_PLUGIN_H
#define __BINIOU_PLUGIN_H

#include <json-c/json.h>
#include "options.h"

#define PLUGIN_PARAMETER_CHANGED 2

enum PluginType { PL_INPUT, PL_MAIN, PL_OUTPUT };

struct Context_s;

typedef struct Plugin_s {
  void   *handle;     /* .so handle */

  uint32_t id;        /* creation date */
  uint32_t version;   /* version to increase when exposed parameters change */
  u_long *options;
  u_long *mode;

  char   *name;
  char   *file;       /* to unload/reload */
  char   *dname;      /* display name */
  char   *desc;       /* plugin description */

  pthread_t thread;

  u_long calls;       /* number of times this plugin was run */
  uint8_t selected_param;

  /* TODO a struct callbacks */
  /* callbacks */
  int8_t (*create)(struct Context_s *);	        /* constructor */
  void   (*destroy)(struct Context_s *);	/* destructor */

  void   (*run)(struct Context_s *);	        /* run function */
  void * (*parameters)(void *);                 /* parameters managment */
  void * (*jthread)(void *);                    /* joinable thread */

  void   (*on_switch_on)(struct Context_s *); 	/* switching on */
  void   (*on_switch_off)(struct Context_s *);	/* switching off */

  /* Output plugin stuff */
  void (*fullscreen)(const int);
  void (*switch_cursor)();
} Plugin_t;

Plugin_t *Plugin_new(const char *, const char *, const enum PluginType);
void Plugin_delete(Plugin_t *);
void Plugin_reload(Plugin_t *);

int8_t Plugin_init(Plugin_t *);

char *Plugin_name(const Plugin_t *);
char *Plugin_dname(const Plugin_t *);

uint8_t plugin_parameter_number(struct json_object *in_parameters);
void plugin_parameter_array_add_int(json_object *params_array, const char *name, int v, int dec, int inc);
void plugin_parameter_array_add_double(json_object *params_array, const char *name, double v, double dec, double inc);
void plugin_parameter_array_add_string_list(json_object *params_array, const char *name, uint32_t nb_elems, const char **elems, uint32_t elem_id);
uint8_t plugin_parameter_parse_int(struct json_object *in_parameters, const char *name, int *value);
uint8_t plugin_parameter_parse_int_range(struct json_object *in_parameters, const char *name, int *value, int vmin, int vmax);
uint8_t plugin_parameter_parse_double(struct json_object *in_parameters, const char *name, double *value);
uint8_t plugin_parameter_parse_float(struct json_object *in_parameters, const char *name, float *value);
uint8_t plugin_parameter_parse_float_range(struct json_object *in_parameters, const char *name, float *value, float vmin, float vmax);
uint8_t plugin_parameter_find_string_in_list(struct json_object *in_parameters, const char *name, int *value);
uint8_t plugin_parameter_parse_string_list_as_int_range(struct json_object *in_parameters, const char *name, uint32_t nb_elems, const char **elems, int *value, int vmin, int vmax);
uint8_t plugin_parameter_parse_string(struct json_object *in_parameters, const char *name, char **value);
struct json_object *plugin_parameter_change_selected(const char *delta);
struct json_object *plugin_parameters_to_saved_parameters(struct json_object *in);

#endif /* __BINIOU_PLUGIN_H */
