/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <json-c/json.h>
#include <inttypes.h>
#include "globals.h"
#include "sequence.h"
#include "images.h"
#include "colormaps.h"

#define SEQUENCE_VERSION 0

static void
Sequence_write_json(const Sequence_t *s, const char *filename, const uint8_t full)
{
  json_object *sequence_j = json_object_new_object();
  json_object_object_add(sequence_j, "version", json_object_new_int(SEQUENCE_VERSION));
  json_object_object_add(sequence_j, "id", json_object_new_int(s->id));

  if (full) {
    json_object_object_add(sequence_j, "auto_colormaps", json_object_new_int(s->auto_colormaps ? 1 : 0));
    if (!s->auto_colormaps)
      json_object_object_add(sequence_j, "colormap", json_object_new_string(Colormaps_name(s->cmap_id)));

    json_object_object_add(sequence_j, "auto_images", json_object_new_int(s->auto_images ? 1 : 0));
    if (!s->auto_images)
      json_object_object_add(sequence_j, "image", json_object_new_string(Images_name(s->image_id)));
  }

  json_object *plugins_j = json_object_new_array();

  /* iterate over plugins list */
  for (GList *layers = g_list_first(s->layers); layers != NULL; layers = g_list_next(layers)) {
    Layer_t *layer = (Layer_t *)layers->data;
    Plugin_t *p = layer->plugin;

    json_object *j_plugin = json_object_new_object();
    json_object_object_add(j_plugin, "lens", json_object_new_int((s->lens != NULL) && (p == s->lens)));
    json_object_object_add(j_plugin, "name", json_object_new_string(p->name));
    json_object_object_add(j_plugin, "id", json_object_new_int(p->id));
    json_object_object_add(j_plugin, "version", json_object_new_int(p->version));
    json_object_object_add(j_plugin, "mode", json_object_new_string(LayerMode_to_string(layer->mode)));

    if (p->parameters) {
      json_object *j_params = (json_object *)p->parameters(NULL);
      json_object_object_add(j_plugin, "parameters", 
        plugin_parameters_to_saved_parameters(j_params));
      json_object_put(j_params);
    }

    json_object_array_add(plugins_j, j_plugin);
  }

  json_object_object_add(sequence_j, "plugins", plugins_j);

  const char *seq_str = json_object_to_json_string_ext(sequence_j, JSON_C_TO_STRING_PRETTY | JSON_C_TO_STRING_SPACED);

  FILE *fp = fopen(filename, "w");
  fputs(seq_str, fp);
  fflush(fp);
  fclose(fp);

  json_object_put(sequence_j);
}


void
Sequence_save(Sequence_t *s, int overwrite, const int is_transient,
              const uint8_t bare, const char auto_colormaps, const char auto_images)
{
  Sequence_t *store = NULL;

  if (g_list_length(s->layers) == 0) {
    printf("[!] *NOT* saving an empty sequence !\n");
    return;
  }

  if (s->broken) {
    printf("[!] Sequence is broken, won't save !\n");
    return;
  }

  if (overwrite && (s->id == 0)) {
    printf("[!] Overwriting a NEW sequence == saving\n");
    overwrite = 0;
  }

  if (!overwrite || is_transient) {
    s->id = unix_timestamp();
  }

  if (s->name != NULL) {
    xfree(s->name);
  }
  s->name = g_strdup_printf("%" PRIu32, s->id);
  printf("[s] Saving sequence %" PRIu32 "\n", s->id);

  const gchar *blah = Sequences_get_dir();
  rmkdir(blah);
  // g_free(blah);

  /* set auto_colormaps from context if needed */
  if (s->auto_colormaps == -1) {
    s->auto_colormaps = auto_colormaps;
  }
  /* set auto_images from context if needed */
  if (s->auto_images == -1) {
    s->auto_images = auto_images;
  }

  char *filename_json = NULL;
  if (overwrite) {
    filename_json = g_strdup_printf("%s/%s.json", blah, s->name);
  } else {
    filename_json = g_strdup_printf("%s/%" PRIu32 ".json", blah, s->id);
  }

  printf("[s] Filename: %s\n", filename_json);

  Sequence_write_json(s, filename_json, bare);
  g_free(filename_json);

  s->changed = 0;

  if (overwrite) {
    GList *oldp = g_list_find_custom(sequences->seqs, (gpointer)s, Sequence_sort_func);
    Sequence_t *old;

    if (oldp != NULL) {
      old = (Sequence_t *)oldp->data;
      Sequence_copy(s, old);
    } else {
      overwrite = 0;
    }
  }

  if (overwrite == 0) {
    /* new sequence */
    store = Sequence_new(0);
    Sequence_copy(s, store);

    sequences->seqs = g_list_prepend(sequences->seqs, (gpointer)store);
    sequences->size++;
  }
}
