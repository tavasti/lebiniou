/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include "biniou.h"
#include "plugin.h"

/* Note: dlsym prevents us from fully compiling in -pedantic..
 * see http://www.trilithium.com/johan/2004/12/problem-with-dlsym/
 */

#ifdef DEBUG
#define B_DLSYM(VAR, SYM)                                         \
  (VAR) = (void (*)(struct Context_s *)) dlsym(p->handle, (SYM)); \
  if (libbiniou_verbose && ((VAR) != NULL))                       \
    printf("[p] >> %s has '%s'\n", p->name, (SYM))
#define B_GOTSYM(VAR, SYM) if ((VAR) != NULL)                     \
    printf("[p] >> %s has '%s'\n", p->name, (SYM));
#else
#define B_DLSYM(VAR, SYM)                                         \
  (VAR) = (void (*)(struct Context_s *)) dlsym(p->handle, (SYM))
#define B_GOTSYM(VAR, SYM) { }
#endif


static Plugin_t *
Plugin_load(Plugin_t *p)
{
  const char *error = NULL;
  u_long *_tmp;

  assert(p != NULL);

  p->handle = dlopen(p->file, RTLD_NOW);

  if (p->handle == NULL) {
    error = dlerror();
    VERBOSE(fprintf(stderr, "[!] Failed to load plugin '%s': %s\n", p->name, error));
    xfree(p->name);
    xfree(p->file);
    xfree(p);

    return NULL;
  } else {
    VERBOSE(printf("[p] Loading plugin '%s'", p->name));
  }
  fflush(stdout);

  _tmp = (u_long *) dlsym(p->handle, "id");
  if (_tmp == NULL) {
    error = dlerror();
    fprintf(stderr, "\n");
    xerror("Plugin MUST define an id (%s)\n", error);
  } else {
    p->id = *_tmp;
    if (libbiniou_verbose)
#ifdef DEBUG
      printf(" (id= %"PRIu32")\n", p->id);
#else
      printf("\n");
#endif
  }

  _tmp = (u_long *) dlsym(p->handle, "options");
  if (_tmp == NULL) {
    error = dlerror();
    xerror("Plugin MUST define options (%s)\n", error);
  } else {
    p->options = _tmp;
  }

  _tmp = (u_long *) dlsym(p->handle, "mode");
  p->mode = _tmp;

  /* get display name */
  p->dname = (char *) dlsym(p->handle, "dname");
  B_GOTSYM(p->dname, "dname");
  if (p->dname == NULL) {
    p->dname = p->name;
  }

  /* get description */
  p->desc = (char *) dlsym(p->handle, "desc");
  B_GOTSYM(p->desc, "desc");

  p->create = (int8_t (*)(struct Context_s *)) dlsym(p->handle, "create");
  B_DLSYM(p->destroy, "destroy");
  B_DLSYM(p->run, "run");
  B_DLSYM(p->on_switch_on, "on_switch_on");
  B_DLSYM(p->on_switch_off, "on_switch_off");

  /* Output plugin stuff */
  p->fullscreen = (void (*)(int)) dlsym(p->handle, "fullscreen");
  B_GOTSYM(p->fullscreen, "fullscreen");
  p->switch_cursor = (void (*)(void)) dlsym(p->handle, "switch_cursor");
  B_GOTSYM(p->switch_cursor, "switch_cursor");

  /* Input plugin stuff (? mainly -to check --oliv3) */
  p->jthread = (void *(*)(void *)) dlsym(p->handle, "jthread");
  B_GOTSYM(p->jthread, "jthread");

  p->parameters = (void *(*)(void *)) dlsym(p->handle, "parameters");

  return p;
}


static void
Plugin_unload(Plugin_t *p)
{
  assert (p != NULL);

  /* FIXME error checking there, ie if plugin fails to destroy */
  if (p->jthread != NULL) {
    VERBOSE(printf("[p] Joining thread from plugin '%s'... ", p->name));
    pthread_join(p->thread, NULL);
  } else {
    if (p->calls) {
      VERBOSE(printf("[p] Unloading plugin '%s' (%li call%s)... ", p->name, p->calls,
                     ((p->calls == 1) ? "" : "s")));
    } else {
      VERBOSE(printf("[p] Unloading plugin '%s'... ", p->name));
    }
  }

  if (p->destroy != NULL) {
    p->destroy(context);
  }

#ifndef DISABLE_DLCLOSE
  VERBOSE(printf("dlclose... "));
  dlclose(p->handle);
#endif

  VERBOSE(printf("done.\n"));
}


Plugin_t *
Plugin_new(const char *directory, const char *name, const enum PluginType type)
{
  Plugin_t *p = xcalloc(1, sizeof(Plugin_t));

  assert(name != NULL);
  assert(directory != NULL);

  p->name = strdup(name);
  p->calls = 0;

  if (type == PL_INPUT) {
    p->file = g_strdup_printf("%s/input/%s/%s.so", directory, name, name);
  } else if (type == PL_MAIN) {
    p->file = g_strdup_printf("%s/main/%s/%s.so", directory, name, name);
  } else if (type == PL_OUTPUT) {
    p->file = g_strdup_printf("%s/output/%s/%s.so", directory, name, name);
  }

  return Plugin_load(p);
}


void
Plugin_delete(Plugin_t *p)
{
  assert(p != NULL);
  Plugin_unload(p);
  xfree(p->name);
  g_free(p->file);
  xfree(p);
}


void
Plugin_reload(Plugin_t *p)
{
  assert(p != NULL);
  Plugin_unload(p);
  Plugin_load(p);
  VERBOSE(printf("[p] Reloaded plugin '%s'\n", p->name));
}


int8_t
Plugin_init(Plugin_t *p)
{
  int8_t res = 1;
  assert(p != NULL);

  if (p->create != NULL) {
    VERBOSE(printf("[+] Initializing plugin %s\n", p->name));
    res = p->create(context);
  }

  if ((p->jthread != NULL) && res) {
    pthread_create(&p->thread, NULL, p->jthread, (void *)context);
    VERBOSE(printf("[p] Launched thread %s\n", p->name));
  }

  return res;
}


static char *
Plugin_uppercase(const char *str)
{
  char *tmp;

  assert(str != NULL);
  tmp = strdup(str);

  assert(tmp != NULL);
  assert(tmp[0] != '\0');
  tmp[0] = (char)toupper((int)tmp[0]);

  return tmp;
}


char *
Plugin_name(const Plugin_t *p)
{
  assert(p != NULL);
  return Plugin_uppercase(p->name);
}


char *
Plugin_dname(const Plugin_t *p)
{
  assert(p != NULL);
  return Plugin_uppercase(p->dname);
}


uint8_t
plugin_parameter_number(struct json_object *in_parameters)
{
  if (json_object_is_type(in_parameters, json_type_array) == 0) {
    return 0;
  }

  return json_object_array_length(in_parameters);
}


void
plugin_parameter_array_add_int(json_object *params_array, const char *name, int v, int dec, int inc)
{
  json_object *param = json_object_new_object();
  json_object_object_add(param, "name", json_object_new_string(name));
  json_object_object_add(param, "value", json_object_new_int(v));
  json_object_object_add(param, "dec", json_object_new_int(dec));
  json_object_object_add(param, "inc", json_object_new_int(inc));
  json_object_array_add(params_array, param);
}


void
plugin_parameter_array_add_double(json_object *params_array, const char *name, double v, double dec, double inc)
{
  json_object *param = json_object_new_object();
  json_object_object_add(param, "name", json_object_new_string(name));
  json_object_object_add(param, "value", json_object_new_double(v));
  json_object_object_add(param, "dec", json_object_new_double(dec));
  json_object_object_add(param, "inc", json_object_new_double(inc));
  json_object_array_add(params_array, param);
}


void
plugin_parameter_array_add_string_list(json_object *params_array, const char *name, uint32_t nb_elems, const char **elems, uint32_t elem_id)
{
  json_object *param = json_object_new_object();
  json_object_object_add(param, "name", json_object_new_string(name));
  json_object_object_add(param, "value", json_object_new_string(elems[elem_id]));
  json_object_object_add(param, "dec", json_object_new_int(-1));
  json_object_object_add(param, "inc", json_object_new_int(1));

  json_object *value_list = json_object_new_array();
  for (uint32_t n = 0; n < nb_elems; n++) {
    json_object_array_add(value_list, json_object_new_string(elems[n]));
  }
  json_object_object_add(param, "value_list", value_list);

  json_object_array_add(params_array, param);
}


uint8_t
plugin_parameter_parse_int(struct json_object *in_parameters, const char *name, int *value)
{
  if (json_object_is_type(in_parameters, json_type_array) == 0) {
    return 0;
  }

  uint8_t N = json_object_array_length(in_parameters);

  for (uint8_t n = 0; n < N; n++) {
    struct json_object *p = json_object_array_get_idx(in_parameters, n);

    json_object *j_name;
    if (json_object_object_get_ex(p, "name", &j_name) &&
        json_object_is_type(j_name, json_type_string)) {
      if (strcmp(json_object_get_string(j_name), name) == 0) {
        json_object *j_value;
        if (json_object_object_get_ex(p, "value", &j_value) &&
            json_object_is_type(j_value, json_type_int)) {
          *value = json_object_get_int(j_value);
          return 1;
        }
      }
    }
  }

  return 0;
}


uint8_t
plugin_parameter_parse_int_range(struct json_object *in_parameters, const char *name, int *value, int vmin, int vmax)
{
  int new_value = 0;
  uint8_t ret = plugin_parameter_parse_int(in_parameters, name, &new_value);

  if (new_value >= vmin && new_value <= vmax && ret == 1) {
    if (*value != new_value) {
      ret |= PLUGIN_PARAMETER_CHANGED;
    }

    *value = new_value;
  }

  return ret;
}


uint8_t
plugin_parameter_parse_double(struct json_object *in_parameters, const char *name, double *value)
{
  if (json_object_is_type(in_parameters, json_type_array) == 0) {
    return 0;
  }

  uint8_t N = json_object_array_length(in_parameters);

  for (uint8_t n = 0; n < N; n++) {
    struct json_object *p = json_object_array_get_idx(in_parameters, n);

    json_object *j_name;
    if (json_object_object_get_ex(p, "name", &j_name) &&
        json_object_is_type(j_name, json_type_string)) {
      if (strcmp(json_object_get_string(j_name), name) == 0) {
        json_object *j_value;
        if (json_object_object_get_ex(p, "value", &j_value) &&
            json_object_is_type(j_value, json_type_double)) {
          *value = json_object_get_double(j_value);
          return 1;
        }
      }
    }
  }

  return 0;
}


uint8_t
plugin_parameter_parse_float(struct json_object *in_parameters, const char *name, float *value)
{
  double new_value;
  uint8_t ret = plugin_parameter_parse_double(in_parameters, name, &new_value);
  if (ret) {
    *value = new_value;
  }

  return ret;
}


uint8_t
plugin_parameter_parse_float_range(struct json_object *in_parameters, const char *name, float *value, float vmin, float vmax)
{
  float new_value = 0;
  uint8_t ret = plugin_parameter_parse_float(in_parameters, name, &new_value);

  if (new_value >= vmin && new_value <= vmax && ret == 1) {
    if (*value != new_value) {
      ret |= PLUGIN_PARAMETER_CHANGED;
    }

    *value = new_value;
  }

  return ret;
}


uint8_t
plugin_parameter_parse_string(struct json_object *in_parameters, const char *name, char **value)
{
  if (json_object_is_type(in_parameters, json_type_array) == 0) {
    return 0;
  }

  uint8_t N = json_object_array_length(in_parameters);

  for (uint8_t n = 0; n < N; n++) {
    struct json_object *p = json_object_array_get_idx(in_parameters, n);

    json_object *j_name;
    if (json_object_object_get_ex(p, "name", &j_name) &&
        json_object_is_type(j_name, json_type_string)) {
      if (strcmp(json_object_get_string(j_name), name) == 0) {
        json_object *j_value;
        if (json_object_object_get_ex(p, "value", &j_value) &&
            json_object_is_type(j_value, json_type_string)) {
          const char *str = json_object_get_string(j_value);
          *value = (char *)str;
          return 1;
        }
      }
    }
  }

  return 0;
}


uint8_t
plugin_parameter_parse_string_list_as_int_range(struct json_object *in_parameters, const char *name,
  uint32_t nb_elems, const char **elems, int *value, int vmin, int vmax)
{
  uint8_t ret       = 0;
  int     new_value = 0;
  char   *str       = NULL;

  if (plugin_parameter_parse_string(in_parameters, name, &str)) {
    for (uint32_t n = 0; n < nb_elems; n++) {
      if (strcmp(elems[n], str) == 0) {
        new_value = n;

        if (new_value >= vmin && new_value <= vmax) {
          ret = 1;
          if (*value != new_value) {
            ret |= PLUGIN_PARAMETER_CHANGED;
          }

          *value = new_value;
        }
      }
    }
  }

  return ret;
}


uint8_t
plugin_parameter_find_string_in_list(struct json_object *in_parameters, const char *name, int *value)
{
  uint8_t ret = 0;

  json_object *j_value;
  json_object_object_get_ex(in_parameters, "value", &j_value);
  const char *str = json_object_get_string(j_value);

  json_object *j_value_list;
  if (json_object_object_get_ex(in_parameters, "value_list", &j_value_list) &&
    json_object_is_type(j_value_list, json_type_array) && str != NULL) {
    for (uint32_t n = 0; n < (uint32_t)json_object_array_length(j_value_list); n++) {
      const char *list_str = json_object_get_string(json_object_array_get_idx(j_value_list, n));

      if (strcmp(list_str, str) == 0) {
        *value = n;
        ret = 1;
        break;
      }
    }
  }

  return ret;
}


struct json_object *
plugin_parameter_change_selected(const char *delta)
{
  struct json_object *ret = NULL;

  if (plugins->selected->parameters != NULL) {
    struct json_object *j_params = plugins->selected->parameters(NULL);
    struct json_object *p = json_object_array_get_idx(j_params, plugins->selected->selected_param);
    struct json_object *j_delta;
    struct json_object *j_value;

    if (json_object_object_get_ex(p, delta, &j_delta) &&
        json_object_object_get_ex(p, "value", &j_value)) {
      if (json_object_is_type(j_delta, json_type_int) &&
          json_object_is_type(j_value, json_type_int)) {
        int delta = json_object_get_int(j_delta);
        int value = json_object_get_int(j_value);

        json_object_object_del(p, "value");
        json_object_object_add(p, "value", json_object_new_int(value + delta));
        ret = plugins->selected->parameters(j_params);

      } else if (json_object_is_type(j_delta, json_type_double) &&
          json_object_is_type(j_value, json_type_double)) {
        double delta = json_object_get_double(j_delta);
        double value = json_object_get_double(j_value);

        json_object_object_del(p, "value");
        json_object_object_add(p, "value", json_object_new_double(value + delta));
        ret = plugins->selected->parameters(j_params);

      } else if (json_object_is_type(j_delta, json_type_int) &&
          json_object_is_type(j_value, json_type_string)) {
        int delta = json_object_get_int(j_delta);
        int list_index = 0;

        if (plugin_parameter_find_string_in_list(p, "value", &list_index)) {
          json_object *j_value_list;
          json_object_object_get_ex(p, "value_list", &j_value_list);
          int N = (int)json_object_array_length(j_value_list);

          int new_value = list_index + delta;
          if (new_value >= N) {
            new_value = new_value % N;
          } else if (new_value < 0) {
            new_value = N + new_value % N;
          }

          const char *str = json_object_get_string(json_object_array_get_idx(j_value_list, new_value));
          json_object_object_del(p, "value");
          json_object_object_add(p, "value", json_object_new_string(str));
          ret = plugins->selected->parameters(j_params);
        }
      }
    }

    json_object_put(j_params);
  }

  return ret;
}


struct json_object *
plugin_parameters_to_saved_parameters(struct json_object *in)
{
  json_object *out = json_object_new_array();

  uint8_t N = json_object_array_length(in);

  for (uint8_t n = 0; n < N; n++) {
    struct json_object *p = json_object_array_get_idx(in, n);

    json_object *j_name;
    if (json_object_object_get_ex(p, "name", &j_name) &&
        json_object_is_type(j_name, json_type_string)) {
      json_object *j_value;
      if (json_object_object_get_ex(p, "value", &j_value)) {
        json_object *param = json_object_new_object();
        json_object_object_add(param, "name", json_object_get(j_name));
        json_object_object_add(param, "value", json_object_get(j_value));
        json_object_array_add(out, param);
      }
    }
  }

  return out;
}
