/*
 *  Copyright 1994-2019 Olivier Girondel
 *  Copyright 2019 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <json-c/json.h>
#include "globals.h"
#include "xmlutils.h"
#include "images.h"
#include "colormaps.h"

#define SEQ_VERSION_MIN 0
#define SEQ_VERSION_MAX 0

Sequence_t *
Sequence_load_json(const char *file)
{
  if (file == NULL) {
    xerror("Attempt to load a sequence with a NULL filename\n");
  }

  char *dot = strrchr(file, '.');
  if ((dot == NULL) || strcasecmp(dot, ".json")) {
#ifdef DEBUG
    printf("[!] Not a sequence filename: '%s'\n", file);
#endif
    return NULL;
  }

#ifdef DEBUG
  printf("[i] Loading sequence from file '%s'\n", file);
#endif

  gchar *file_with_path = g_strdup_printf("%s/%s", Sequences_get_dir(), file);
  FILE *fp = fopen(file_with_path, "r");
  g_free(file_with_path);

  if (fp == NULL) {
    xperror("fopen");
    return NULL;
  }

  fseek(fp, 0, SEEK_END);
  const long file_size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  char *buffer = xmalloc(file_size + 1);
  int n_read = fread(buffer, 1, file_size, fp);
  fclose(fp);
  buffer[n_read] = 0;

  struct json_object *parsed_json = json_tokener_parse(buffer);
  xfree(buffer);

  Sequence_t *s = NULL;
  struct json_object *j_plugins;

  struct json_object *j_sequence_version;
  json_object_object_get_ex(parsed_json, "version", &j_sequence_version);
  int sequence_version = json_object_get_int(j_sequence_version);
  if (sequence_version < SEQ_VERSION_MIN || sequence_version > SEQ_VERSION_MAX) {
    VERBOSE(printf("[!] Sequence version '%d' not supported\n", sequence_version));
    goto error;
  }

  struct json_object *j_id;
  json_object_object_get_ex(parsed_json, "id", &j_id);

  s = Sequence_new(json_object_get_int(j_id));
  *dot = '\0';
  s->name = strdup(file);

  /* get auto_colormaps */
  struct json_object *j_auto_colormaps;
  if (json_object_object_get_ex(parsed_json, "auto_colormaps", &j_auto_colormaps)) {
    s->auto_colormaps = json_object_get_int(j_auto_colormaps);
  } else {
    s->auto_colormaps = 0;
    goto bare_sequence;
  }
  assert((s->auto_colormaps == 0) || (s->auto_colormaps == 1));
#ifdef DEBUG
  printf("[i] Random colormaps: %s\n", (s->auto_colormaps ? "on" : "off"));
#endif

  /* if not auto*, get colormap name */
  if (!s->auto_colormaps) {
    struct json_object *j_cmap;
    json_object_object_get_ex(parsed_json, "colormap", &j_cmap);
    const char *cmap = json_object_get_string(j_cmap);
#ifdef DEBUG
    printf("[i] Need colormap: '%s'\n", cmap);
#endif
    s->cmap_id = Colormaps_find(cmap);
    // xfree(cmap);
  } else {
    s->cmap_id = Colormaps_random_id();
  }

  /* get auto_images */
  struct json_object *j_auto_images;
  if (json_object_object_get_ex(parsed_json, "auto_images", &j_auto_images)) {
    s->auto_images = json_object_get_int(j_auto_images);
  } else {
    s->auto_images = 0;
  }
  assert((s->auto_images == 0) || (s->auto_images == 1));
#ifdef DEBUG
  printf("[i] Random images: %s\n", (s->auto_images ? "on" : "off"));
#endif

  /* if not auto*, get image name */
  if (!s->auto_images) {
    struct json_object *j_image;
    json_object_object_get_ex(parsed_json, "image", &j_image);
    const char *image = json_object_get_string(j_image);
#ifdef DEBUG
    printf("[i] Need image: '%s'\n", image);
#endif
    if (images == NULL) {
      VERBOSE(printf("[!] No images are loaded, won't find '%s'\n", image));
      goto error;
    } else {
      s->image_id = Images_find(image);
      if (s->image_id == 0) {
        VERBOSE(printf("[!] Image '%s' not found, using default\n", image));
      }
    }
  } else {
    if (images == NULL) {
      s->broken      = 1;
      s->image_id    = -1;
      s->auto_images = 0;
    } else {
      s->image_id = Images_random_id();
    }
  }

bare_sequence:

  /* get plugins */
  json_object_object_get_ex(parsed_json, "plugins", &j_plugins);
  unsigned n_plugins = json_object_array_length(j_plugins);

  for (u_short n = 0; n < n_plugins; n++) {
    struct json_object *j_p = json_object_array_get_idx(j_plugins, n);

    struct json_object *j_p_id;
    json_object_object_get_ex(j_p, "id", &j_p_id);
    int id = json_object_get_int(j_p_id);
    if (!id) {
      goto error;
    }

    struct json_object *j_p_version;
    json_object_object_get_ex(j_p, "version", &j_p_version);
    int version = json_object_get_int(j_p_version);
    VERBOSE(printf("[!] Plugin version: %d\n", version));

    Plugin_t *p = Plugins_find((u_long)id);
    if (p == NULL) {
      goto error;
    }

    struct json_object *j_p_mode;
    json_object_object_get_ex(j_p, "mode", &j_p_mode);
    const char *mode_str = json_object_get_string(j_p_mode);

    Layer_t *layer = Layer_new(p);
    layer->mode    = LayerMode_from_string(mode_str);

    struct json_object *j_p_lens;
    json_object_object_get_ex(j_p, "lens", &j_p_lens);
    if (json_object_get_int(j_p_lens)) {
      s->lens = p;
    }

    struct json_object *j_p_parameters;
    if (json_object_object_get_ex(j_p, "parameters", &j_p_parameters)) {
      layer->plugin_parameters = json_object_get(j_p_parameters);
    }

    s->layers = g_list_append(s->layers, (gpointer)layer);
  }

  json_object_put(parsed_json);
  return s;

error:
  VERBOSE(printf("[!] Failed to load sequence from file '%s'\n", file));
  Sequence_delete(s);

  json_object_put(parsed_json);
  return NULL;
}

/*
 * Left as an exercise to the reader: this code is not robust at all
 * we expect to read files that have been writen by Sequence_write.
 * But we are talibans and are not kind with wrong inputs.
 */
Sequence_t *
Sequence_load_xml(const char *file)
{
  Sequence_t *s = NULL;
  xmlDocPtr doc = NULL; /* XmlTree */
  xmlNodePtr sequence_node = NULL, sequence_node_save = NULL, plugins_node = NULL;
  int res;
  long tmp;
  xmlChar *youhou;
  int legacy = 0;

  if (file == NULL) {
    xerror("Attempt to load a sequence with a NULL filename\n");
  }

  char *dot = strrchr(file, '.');
  if ((dot == NULL) || strcasecmp(dot, ".xml")) {
#ifdef DEBUG
    printf("[!] Not a sequence filename: '%s'\n", file);
#endif
    return NULL;
  }

#ifdef DEBUG
  printf("[i] Loading sequence from file '%s'\n", file);
#endif

  /* BLA ! */
  xmlKeepBlanksDefault(0);
  xmlSubstituteEntitiesDefault(1);

  /*
   * build an XML tree from the file
   */
  gchar *blah = g_strdup_printf("%s/%s", Sequences_get_dir(), file);

  doc = xmlParseFile(blah);
  g_free(blah);
  if (doc == NULL) {
    xerror("xmlParseFile error\n");
  }

  sequence_node = xmlDocGetRootElement(doc);
  if (sequence_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: xmlDocGetRootElement error\n", file));
    goto error;
  }

  sequence_node = xmlFindElement("sequence", sequence_node);
  if (sequence_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: no <sequence> found\n", file));
    goto error;
  }

  youhou = xmlGetProp(sequence_node, (const xmlChar *)"id");
  tmp    = getintfield(youhou);
  xmlFree(youhou);

  if (tmp <= 0) {
    VERBOSE(printf("[!] Sequence %s: id must be > 0\n", file));
    goto error;
  }

  s = Sequence_new(tmp);

  /* first, get <auto_colormaps> */
  sequence_node_save = sequence_node = sequence_node->xmlChildrenNode;

  sequence_node = xmlFindElement("auto_colormaps", sequence_node);
  if (sequence_node == NULL) {
    sequence_node = sequence_node_save;
    goto bare_sequence;
  }
  res = xmlGetOptionalLong(doc, sequence_node, &tmp);
  if (res == -1) {
    s->auto_colormaps = 0;
  } else {
    s->auto_colormaps = (u_char)tmp;
  }
  assert((s->auto_colormaps == 0) || (s->auto_colormaps == 1));
#ifdef DEBUG
  printf("[i] Random colormaps: %s\n", (s->auto_colormaps ? "on" : "off"));
#endif

  if (!s->auto_colormaps) {
    /* not auto*, get colormap name */
    char *cmap = xmlGetMandatoryString(doc, "colormap", sequence_node);
#ifdef DEBUG
    printf("[i] Need colormap: '%s'\n", cmap);
#endif
    s->cmap_id = Colormaps_find(cmap);
    xfree(cmap);
  } else {
    s->cmap_id = Colormaps_random_id();
  }

  /* then, get  <auto_images> or (legacy) <auto_pictures> */
  sequence_node_save = sequence_node = sequence_node->next;
  sequence_node = xmlFindElement("auto_images", sequence_node);
  if (sequence_node == NULL) {
    sequence_node = sequence_node_save;
    sequence_node = xmlFindElement("auto_pictures", sequence_node);
    if (sequence_node == NULL) {
      VERBOSE(printf("[!] Sequence %s: no <auto_images> or <auto_pictures> found\n", file));
      goto error;
    } else {
      legacy = 1;
    }
  }
  res = xmlGetOptionalLong(doc, sequence_node, &tmp);
  if (res == -1) {
    s->auto_images = 0;
  } else {
    s->auto_images = (u_char)tmp;
  }
  assert((s->auto_images == 0) || (s->auto_images == 1));
#ifdef DEBUG
  printf("[i] Random images: %s\n", (s->auto_images ? "on" : "off"));
#endif

  if (!s->auto_images) {
    /* not auto*, get image name */
    char *image = xmlGetMandatoryString(doc, legacy ? "picture" : "image", sequence_node);
#ifdef DEBUG
    printf("[i] Need image: '%s'\n", image);
#endif
    if (images == NULL) {
      VERBOSE(printf("[!] No images are loaded, won't find '%s'\n", image));
      xfree(image);
      goto error;
    } else {
      s->image_id = Images_find(image);
      if (s->image_id == 0) {
        VERBOSE(printf("[!] Image '%s' not found, using default\n", image));
      }
      xfree(image);
    }
  } else {
    if (images == NULL) {
      s->broken      = 1;
      s->image_id    = -1;
      s->auto_images = 0;
    } else {
      s->image_id = Images_random_id();
    }
  }

bare_sequence:
  /* now, get plugins */
  plugins_node = xmlFindElement("plugins", sequence_node);
  if (plugins_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: no <plugins> found\n", file));
    goto error;
  }
  plugins_node = plugins_node->xmlChildrenNode;
  if (plugins_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: no elements in <plugins>\n", file));
    goto error;
  }

  while (plugins_node != NULL) {
    u_char   lens;
    Layer_t *layer;
    Plugin_t *p;

    assert(plugins_node->name != NULL);
    lens = !xmlStrcmp(plugins_node->name, (const xmlChar *)"lens");

    youhou = xmlGetProp(plugins_node, (const xmlChar *)"id");
    tmp    = getintfield(youhou);
    xmlFree(youhou);
    if (!tmp) {
      goto error;
    }

    p = Plugins_find((u_long)tmp);
    if (p == NULL) {
      goto error;
    }

    enum LayerMode mode;

    layer = Layer_new(p);

    youhou = xmlGetProp(plugins_node, (const xmlChar *)"mode");
    if (youhou != NULL) {
      mode = LayerMode_from_string((const char *)youhou);
      xmlFree(youhou);
    } else {
      mode = NORMAL;
    }
    layer->mode = mode;

    s->layers = g_list_append(s->layers, (gpointer)layer);

    if (lens) {
      s->lens = (Plugin_t *)p;
    }

    plugins_node = plugins_node->next;
  }

  /* Check sequence length */
  assert(Sequence_size(s) <= MAX_SEQ_LEN);

  /* Clean up */
  xmlFreeDoc(doc);
  xmlCleanupParser(); /* FIXME is this ok ? */

  *dot = '\0'; /* spr0tch */
  s->name = strdup(file);

  return s;

error:
  VERBOSE(printf("[!] Failed to load sequence from file '%s'\n", file));

  /* Clean up */
  xmlFreeDoc(doc);
  xmlCleanupParser(); /* FIXME is this ok ? */

  Sequence_delete(s);

  return NULL;
}

Sequence_t *
Sequence_load(const char *filename)
{
  char *dot = strrchr(filename, '.');
  if (dot != NULL) {
    if (strncasecmp(dot, ".json", 5 * sizeof(char)) == 0) {
      return Sequence_load_json(filename);
    }

    if (strncasecmp(dot, ".xml", 4 * sizeof(char)) == 0) {
      return Sequence_load_xml(filename);
    }
  }

  return NULL;
}
