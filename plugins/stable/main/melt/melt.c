/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 1178827232;
u_long options = BE_DISPLACE;
char desc[] = "Melt effect";


static int speed = 0;
static float color_factor = 0;

json_object *
get_parameters()
{
  json_object *params_array = json_object_new_array();
  plugin_parameter_array_add_int(params_array, "speed", speed, -1, 1);
  plugin_parameter_array_add_double(params_array, "color_factor", color_factor, -0.01, 0.01);

  return params_array;
}


void
set_parameters(struct json_object *in_parameters)
{
  plugin_parameter_parse_int_range(in_parameters, "speed", &speed, 2, 7);
  plugin_parameter_parse_float_range(in_parameters, "color_factor", &color_factor, 0, 1);
}


void *
parameters(void *in_parameters)
{
  set_parameters((struct json_object*)in_parameters);

  return (void *)get_parameters();
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  speed = 3;
  color_factor = 0.5;
}


void
run(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  int x, y;

  for (x = 0; x < WIDTH; x++) {
    set_pixel_nc(dst, x, 0, get_pixel_nc(src, x, 0));
  }

  for (y = 1; y < HEIGHT; y++) {
    for (x = 0; x < WIDTH; x++) {
      Pixel_t c = get_pixel_nc(src, x, y);
      int y2 = y - (c >> (8-speed));

      if (y2 < 0) {
        y2 = 0;
      }

      set_pixel_nc(dst, x, y, c * color_factor);
      set_pixel_nc(dst, x, y2, c);
    }
  }
  h_line_nc(dst, MAXY, 0, MAXX, 0);
}
