/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "../include/infinity.h"

u_long id = 1190050161;
u_long options = BE_DISPLACE;
char dname[] = "Speaker";
char desc[] = "Infinity effect which reacts to volume";


#define NB_FCT 10

static VectorField_t *vf = NULL;


static float volume_scale = 0;

json_object *
get_parameters()
{
  json_object *params_array = json_object_new_array();
  plugin_parameter_array_add_double(params_array, "volume_scale", volume_scale, -0.1, 0.1);

  return params_array;
}


void
set_parameters(struct json_object *in_parameters)
{
  plugin_parameter_parse_float_range(in_parameters, "volume_scale", &volume_scale, 0, 100);
}


void *
parameters(void *in_parameters)
{
  set_parameters((struct json_object*)in_parameters);

  return (void *)get_parameters();
}


static t_complex
fct(t_complex a, guint32 n, gint32 p1, gint32 p2)
{
  t_complex b;
  float fact;
  float an;
  float circle_size;
  float speed;
  float co, si;
  float nn = (float)n/9.0;

  a.x -= HWIDTH;
  a.y -= HHEIGHT;

  an = 0.015*(p1-2*nn)+0.002;
  co = cosf(an);
  si = sinf(an);

  circle_size = HEIGHT*nn*2;
  speed = (float)4000-p2*1000;

  b.x = (co*a.x-si*a.y);
  b.y = (si*a.x+co*a.y);

  fact = (sqrtf(b.x*b.x+b.y*b.y)-circle_size)/speed+1;
  b.x *= fact;
  b.y *= fact;

  b.x += HWIDTH;
  b.y += HHEIGHT;

  /* because infinity access pixels at (b.x + 1, b.y + 1) */
  b.x = MIN( MAX(b.x, 0), MAXX-1);
  b.y = MIN( MAX(b.y, 0), MAXY-1);

  return b;
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 1;
}


int8_t
create(Context_t *ctx)
{
  vf = VectorField_new(NB_FCT, &fct);

  return 1;
}


void
destroy(Context_t *ctx)
{
  if (vf != NULL) {
    VectorField_delete(vf);
  }
}


void
run(Context_t *ctx)
{
  /* volume must be between 0 and NB_FCT-1 */
  u_char volume = volume = (u_char)(Input_get_volume(ctx->input) * volume_scale * NB_FCT);

  if (volume >= NB_FCT) {
    volume = NB_FCT-1;
  }

  VectorField_run(vf, ctx, volume);
}
