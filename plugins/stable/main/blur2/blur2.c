/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "image_filter.h"


u_long id = 944355800;
u_long options = BE_BLUR;
char desc[] = "Blur filter";


static enum BorderMode border_mode;

json_object *
get_parameters()
{
  json_object *params_array = json_object_new_array();
  plugin_parameter_array_add_string_list(params_array, "border_mode", BM_NB, border_list, border_mode);

  return params_array;
}


void
set_parameters(struct json_object *in_parameters)
{
  int border_mode_idx = 0;
  if (plugin_parameter_parse_string_list_as_int_range(in_parameters, "border_mode", BM_NB, border_list, &border_mode_idx, 0, BM_NB-1)) {
    border_mode = (enum BorderMode)border_mode_idx;
  }
}


void *
parameters(void *in_parameters)
{
  set_parameters((struct json_object*)in_parameters);

  return (void *)get_parameters();
}


void
on_switch_on(Context_t *ctx)
{
  border_mode = BM_TOROIDAL;
}


void
run(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  image_filter_average(dst, src, FT_BLUR2_3x3, border_mode, 0, NULL);
}
