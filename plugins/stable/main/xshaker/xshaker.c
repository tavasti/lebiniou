/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "../include/shaker.h"

u_long id = 947072846;
u_long options = BE_LENS;
char dname[] = "X Shaker";
char desc[] = "Shaker effect";

static float volume_scale = 0;

json_object *
get_parameters()
{
  json_object *params_array = json_object_new_array();
  plugin_parameter_array_add_double(params_array, "volume_scale", volume_scale, -0.01, 0.01);

  return params_array;
}


void
set_parameters(struct json_object *in_parameters)
{
  plugin_parameter_parse_float_range(in_parameters, "volume_scale", &volume_scale, 0, 100);
}


void *
parameters(void *in_parameters)
{
  set_parameters((struct json_object*)in_parameters);

  return (void *)get_parameters();
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 1;
}


void
run(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  uint16_t i;

  Buffer8_init_mask_3x3(active_buffer(ctx));

  pthread_mutex_lock(&ctx->input->mutex);
  for (i = 0; i < WIDTH; i++) {
    u_short idx = 0;
    uint16_t j;

    for (j = 0; j < HEIGHT; j++) {
      char dx = Input_clamp(ctx->input->data[A_LEFT][idx])  * volume_scale * SOUND_DISTURB_K;
      char dy = Input_clamp(ctx->input->data[A_RIGHT][idx]) * volume_scale * SOUND_DISTURB_K;

      if (++idx == ctx->input->size) {
        idx = 0;
      }

      displace(src, dst, i, j, i + dx, j + dy);
    }
  }
  pthread_mutex_unlock(&ctx->input->mutex);
}
