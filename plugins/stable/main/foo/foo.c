/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/* begin blabla intro */

/*
 * This is a very simple plugin that will show you:
 *
 * . what is necessary to define a new plugin
 * . basic 2d graphic functions
 * . access to the input/sound buffer
 * . plugin modes & options
 *
 */

/* You first need to include this file */
#include "context.h"


/* The plugin's id. MUST be unique.
 * Convention is to use the UNIX timestamp of when you
 * start coding the plugin, basically the $ date "+%s"
 * command in the shell */
u_long id = 944338581;

/* options */
u_long options = BE_GFX | BEQ_UNIQUE | BEQ_NORANDOM;

/* default layer mode */
u_long mode = OVERLAY;

/* display name in the OSD */
char dname[] = "The foo plugin";

/* description for the OSD when plugin is selected */
char desc[] = "Basic/demo plugin";


static u_short x, y;


// you don't have to define this function if unneeded
int8_t
create(Context_t *ctx)
{
  // here goes the code when the plugin is loaded
  x = CENTERX;
  y = CENTERY;

  // return 1 on success, 0 if the plugin failed to initialize
  return 1;
}


// you don't have to define this function if unneeded
void
destroy(Context_t *ctx)
{
  // here goes the code when the plugin is unloaded
}


// the main plugin function
void
run(Context_t *ctx)
{
  static u_short idx = 0;
  u_short steps, s;
  Buffer8_t *dst;

  steps = (u_short)(drand48() * 100);
  dst = passive_buffer(ctx);

  Buffer8_clear(dst);

  // make sure to lock/unlock the input mutex if the plugin uses audio buffers
  pthread_mutex_lock(&ctx->input->mutex);

  if (drand48() < .5) {
    for (s = 0; s < steps; s++) {
      Pixel_t c = ctx->input->data_u[A_MONO][idx] * 255;

      set_pixel_nc(dst, x++, y, c);

      if (x == WIDTH) {
        x = 0;
        if (++y == HEIGHT) {
          y = 0;
        }
      }

      if (++idx == ctx->input->size) {
        idx = 0;
      }
    }
  } else {
    for (s = 0; s < steps; s++) {
      Pixel_t c = ctx->input->data_u[A_MONO][idx] * 255;

      set_pixel_nc(dst, x, y++, c);

      if (y == HEIGHT) {
        y = 0;
        if (++x == WIDTH) {
          x = 0;
        }
      }

      if (++idx == ctx->input->size) {
        idx = 0;
      }
    }
  }

  pthread_mutex_unlock(&ctx->input->mutex);
}
