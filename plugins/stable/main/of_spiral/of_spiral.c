/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"

u_long id = 1057526835;
u_long options = BE_SFX2D;
char dname[] = "OF Spiral";
u_long mode = OVERLAY;
char desc[] = "Pulsing spiral";

static float volume_scale = 0;

json_object *
get_parameters()
{
  json_object *params_array = json_object_new_array();
  plugin_parameter_array_add_double(params_array, "volume_scale", volume_scale, -0.01, 0.01);

  return params_array;
}


void
set_parameters(struct json_object *in_parameters)
{
  plugin_parameter_parse_float_range(in_parameters, "volume_scale", &volume_scale, 0, 1);
}


void *
parameters(void *in_parameters)
{
  set_parameters((struct json_object*)in_parameters);

  return (void *)get_parameters();
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 1;
}


void
run(Context_t *ctx)
{
  static Pixel_t c = 0;
  int i, j;
  float x, y, /*z,*/ theta, phi, r, d, x1, x2, x3 = 5;
  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_clear(dst);

  x1 = Input_get_volume(ctx->input) * WIDTH * volume_scale; /* Nb de pts par droite */
  x2 = 0.3 + ctx->input->data[A_MONO][((int)x1 % ctx->input->size)];

  for (i = 0; i < WIDTH; i++) {
    theta = i / 20.0;   /* Rotation angle */

    r = expf(theta*x2); /* Distance from axis  */
    d = 0.33 * r;       /* Radius relative to r (involute/evolute) */
    /* For ornament, add a periodic function to d */
    for (j = 0; j < 30; j++) {
      phi = 2.0 * M_PI *j / x3;
      x = d * cosf(phi) *cosf(theta);
      y = d * cosf(phi) *sinf(theta);
      x += (r * cosf(theta)) + HWIDTH;
      y += (r * sinf(theta)) + HHEIGHT;
      set_pixel(dst, x, y,  c);
      c++;
      c &= 255;
    }
  }
}
