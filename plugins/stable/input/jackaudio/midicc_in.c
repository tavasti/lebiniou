/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "xmlutils.h"
#include "event_enums.h"

typedef struct {
  xmlChar subsystem;
  xmlChar command;
  xmlChar argument;
  int activated;
} command_cc;

int
Map_command(xmlChar *str) {
  char * string;
  string = (char *) str;
  if (!strcmp(string, "BT_NONE")       )   { return BT_NONE; }
  if (!strcmp(string, "BT_IMAGEFADER") )   { return BT_IMAGEFADER; }
  if (!strcmp(string, "BT_CMAPFADER")  )   { return BT_CMAPFADER; }
  if (!strcmp(string, "BT_PLUGINS")    )   { return BT_PLUGINS; }
  if (!strcmp(string, "BT_CONTEXT")    )   { return BT_CONTEXT; }
  if (!strcmp(string, "BT_SEQMGR")     )   { return BT_SEQMGR; }
  if (!strcmp(string, "BT_LAST")       )   { return BT_LAST; }

  if (!strcmp(string, "BC_NONE")       )   { return BC_NONE; }           
  if (!strcmp(string, "BC_QUIT")       )   { return BC_QUIT; }        
  if (!strcmp(string, "BC_PREV")       )   { return BC_PREV; }        
  if (!strcmp(string, "BC_NEXT")       )   { return BC_NEXT; }        
  if (!strcmp(string, "BC_RANDOM")     )   { return BC_RANDOM; }      
  if (!strcmp(string, "BC_SWITCH")     )   { return BC_SWITCH; }      
  if (!strcmp(string, "BC_RESET")      )   { return BC_RESET; }      
  if (!strcmp(string, "BC_SELECT")     )   { return BC_SELECT; }      
  if (!strcmp(string, "BC_RELOAD")     )   { return BC_RELOAD; }      
  if (!strcmp(string, "BC_SET")        )   { return BC_SET; }         
  if (!strcmp(string, "BC_INFO")       )   { return BC_INFO; }        
  if (!strcmp(string, "BC_MOVE")       )   { return BC_MOVE; }        
  if (!strcmp(string, "BC_SAVE")       )   { return BC_SAVE; }        
  if (!strcmp(string, "BC_USE_BANKSET"))   { return BC_USE_BANKSET; } 
  if (!strcmp(string, "BC_STORE_BANK") )   { return BC_STORE_BANK; } 
  if (!strcmp(string, "BC_USE_BANK")   )   { return BC_USE_BANK; }    
  if (!strcmp(string, "BC_CLEAR_BANK") )   { return BC_CLEAR_BANK; } 
  if (!strcmp(string, "BC_SAVE_BANKS") )   { return BC_SAVE_BANKS; }  
  if (!strcmp(string, "BC_SET_BANKMODE"))  { return BC_SET_BANKMODE; }
  if (!strcmp(string, "BC_LAST")        )  { return BC_LAST; }        
  if (!strcmp(string, "BC_LOCK")       )   { return BC_LOCK; }        
  if (!strcmp(string, "BC_VOLUME_SCALE"))  { return BC_VOLUME_SCALE; }
                      
  if (!strcmp(string, "BA_NONE")       )   { return  BA_NONE; }         
  if (!strcmp(string, "BA_RANDOM")     )   { return  BA_RANDOM; }       
  if (!strcmp(string, "BA_SEQUENCE")   )   { return  BA_SEQUENCE; }      
  if (!strcmp(string, "BA_SCHEME")     )   { return  BA_SCHEME; }        
  if (!strcmp(string, "BA_COLORMAPS")  )   { return  BA_COLORMAPS; }     
  if (!strcmp(string, "BA_IMAGES")     )   { return  BA_IMAGES; }        
  if (!strcmp(string, "BA_FULLSCREEN") )   { return  BA_FULLSCREEN; }    
  if (!strcmp(string, "BA_ROTATIONS")  )   { return  BA_ROTATIONS; }     
  if (!strcmp(string, "BA_BOUNDARY")   )   { return  BA_BOUNDARY; }      
  if (!strcmp(string, "BA_OSD_CMAP")   )   { return  BA_OSD_CMAP; }      
  if (!strcmp(string, "BA_CURSOR")     )   { return  BA_CURSOR; }        
  if (!strcmp(string, "BA_UP")         )   { return  BA_UP; }            
  if (!strcmp(string, "BA_SAVE")       )   { return  BA_SAVE; }          
  if (!strcmp(string, "BA_SCREENSHOT") )   { return  BA_SCREENSHOT; }    
  if (!strcmp(string, "BA_DOWN")       )   { return  BA_DOWN; }          
  if (!strcmp(string, "BA_PREV")       )   { return  BA_PREV; }          
  if (!strcmp(string, "BA_NEXT")       )   { return  BA_NEXT; }          
  if (!strcmp(string, "BA_LAYER_MODE") )   { return  BA_LAYER_MODE; }    
  if (!strcmp(string, "BA_SELECTED")   )   { return  BA_SELECTED; }      
  if (!strcmp(string, "BA_LENS")       )   { return  BA_LENS; }          
  if (!strcmp(string, "BA_BYPASS")     )   { return  BA_BYPASS; }        
  if (!strcmp(string, "BA_WEBCAM")     )   { return  BA_WEBCAM; }        
  if (!strcmp(string, "BA_PULSE")      )   { return  BA_PULSE; }         
  if (!strcmp(string, "BA_LAST")       )   { return  BA_LAST; }          
  if (!strcmp(string, "BA_DELAY")      )   { return  BA_DELAY; }         
  if (!strcmp(string, "BA_SPAN")       )   { return  BA_SPAN; }          
  if (!strcmp(string, "BA_MUTE")       )   { return  BA_MUTE; }          
  if (!strcmp(string, "BA_SEQUENCE_FULL")) { return  BA_SEQUENCE_FULL; }  
  if (!strcmp(string, "BA_SEQUENCE_BARE")) { return  BA_SEQUENCE_BARE; }  
  if (!strcmp(string, "BA_OVERWRITE_FULL")){ return  BA_OVERWRITE_FULL; }  
  if (!strcmp(string, "BA_OVERWRITE_BARE")){ return  BA_OVERWRITE_BARE; }

  return 0;
}                                   
                                    
void
Midi_commands_load(const char *file, command_cc ccs[128][128])
{
  xmlDocPtr doc;
  xmlNodePtr midi_node;
  xmlNodePtr commands_node;
  xmlChar *xmlfield;
  int controller;
  int value;
  command_cc c;

  printf("[i] JACK-MIDI: Loading Midi CC options from file '%s'\n", file);

  FILE *f;
  if (!(f = fopen(file, "r")))
  {
      printf("[!] JACK-MIDI: Config file does not exists '%s'\n", file);
      return;
  }
  fclose(f);

  doc = xmlParseFile(file);
  if (doc == NULL) {
    printf("[!] Not commands defined in '%s'\n", file);
    return;
  }

  midi_node = xmlDocGetRootElement(doc);
  if (midi_node == NULL) {
    VERBOSE(printf("[!] JACK-MIDI %s: xmlDocGetRootElement error\n", file));
    return;
  }
  
  midi_node = xmlFindElement("midi_input", midi_node);
  if (midi_node == NULL) {
    VERBOSE(printf("[!] JACK-MIDI %s: no <midi_input> found\n", file));
    return;
  }
  midi_node = midi_node->xmlChildrenNode;
  
  commands_node = xmlFindElement("command_channels", midi_node);
  if (commands_node == NULL) {
    VERBOSE(printf("[!] JACK-MIDI %s: no <command_channels> found\n", file));
    return;
  }
  commands_node = commands_node->xmlChildrenNode;
  if (commands_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: no elements in <command_channels>\n", file));
    return;
  }


  printf("[i] JACK-MIDI: reading commands from file '%s'\n", file);
  while (commands_node != NULL) {

    xmlfield = xmlGetProp(commands_node, (xmlChar *) "controller");
    controller = getintfield(xmlfield);
    xmlFree(xmlfield);

    xmlfield = xmlGetProp(commands_node, (const xmlChar *) "value");
    value = getintfield(xmlfield);
    xmlFree(xmlfield);

    c.subsystem = Map_command(xmlGetProp(commands_node, (const xmlChar *)"subsystem"));
    c.command =  Map_command(xmlGetProp(commands_node, (const xmlChar *)"command"));
    c.argument =  Map_command(xmlGetProp(commands_node, (const xmlChar *)"argument"));
    c.activated =  1;

    ccs[controller][value] = c;
    commands_node = commands_node->next;

  }
}
 
